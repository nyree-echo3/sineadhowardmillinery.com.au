<?php

namespace App\Http\Controllers\Site;

use App\ContactMessages;
use App\Http\Controllers\Controller;
use App\Mail\ContactMessageAdmin;
use App\Mail\ContactMessageUser;
use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Newsletter;
use Validator, Illuminate\Support\Facades\Input, Redirect;

class ContactController extends Controller
{
    public function index(){

        $fields = Setting::where('key', '=', 'contact-form-fields')->first();

        // Contact Details
        $contact_details = Setting::where('key', '=', 'contact-details')->first();

        $form = $fields->value;

        if(old('name')){
            $fields = json_decode($fields->value);
            foreach ($fields as $field){
                $field->value = old($field->name);
            }

            $form = json_encode($fields);
        }

        return view('site/contact/contact', array(
            'form' => $form,
            'contact_details' => $contact_details->value
        ));
    }

    public function saveMessage(Request $request)
    {

        $rules = array(
            'g-recaptcha-response' => 'required|recaptcha'
        );

        $messages = [
            'g-recaptcha-response.required' => 'Please verify yourself',
            'g-recaptcha-response.recaptcha' => 'Please verify yourself'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('contact')->withErrors($validator)->withInput();
        }

        $fields = Setting::where('key', '=', 'contact-form-fields')->first();
        $fields = json_decode($fields->value);

        $data = array();

        foreach ($fields as $field){

            if($field->type!='header' && $field->type!='paragraph'){

                if($field->type=='date'){
                    $date_array = explode('-',$request->{$field->name});
                     $request->{$field->name} = $date_array[2].'-'.$date_array[1].'-'.$date_array[0];
                }

                $data_field['label'] = $field->label;
                $data_field['field'] = $field->name;
                $data_field['value'] = $request->{$field->name};
                array_push($data, $data_field);
            }
        }

        $message = new ContactMessages();
        $message->data = json_encode($data);
        $message->save();

		$setting = Setting::where('key','=','contact-email')->first();
		$contactEmail = $setting->value;
		
		// Email Website Owner
        Mail::to($contactEmail)->send(new ContactMessageAdmin($message));
		
		// Email User
        Mail::to($request->email)->send(new ContactMessageUser($message));

        //Add Mailchimplist
        $name = trim($request->name);
        $last_name = (strpos($name, ' ') === false) ? '' : preg_replace('#.*\s([\w-]*)$#', '$1', $name);
        $first_name = trim( preg_replace('#'.$last_name.'#', '', $name ) );

        Newsletter::subscribeOrUpdate($request->email, ['FNAME'=>$first_name, 'LNAME'=>$last_name]);

        return \Redirect::to('contact/success');
    }

    public function success(){

        return view('site/contact/success');
    }
}
