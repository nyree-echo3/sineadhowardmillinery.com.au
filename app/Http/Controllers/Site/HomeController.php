<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Jenssegers\Agent\Agent;


class HomeController extends Controller
{
    public function index(){

        $agent = new Agent();

        $cycle_carousel_visible = 3;

        if($agent->isMobile()){
            $cycle_carousel_visible = 1;
        }

    	return view('site/index', array(            						
			'page_type' => "home",
			'cycle_carousel_visible' => $cycle_carousel_visible
        ));

    }
	
	public function styleGuide(){

    	return view('site/style-guide');

    }
}
