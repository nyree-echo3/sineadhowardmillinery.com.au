<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class StatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {        
		DB::table('states')->insert([           
            'name' => 'VIC'          
        ]);
		DB::table('states')->insert([           
            'name' => 'NSW'          
        ]);
		DB::table('states')->insert([           
            'name' => 'QLD'          
        ]);
		DB::table('states')->insert([           
            'name' => 'ACT'          
        ]);
		DB::table('states')->insert([           
            'name' => 'NT'          
        ]);
		DB::table('states')->insert([           
            'name' => 'WA'          
        ]);
		DB::table('states')->insert([           
            'name' => 'SA'          
        ]);
		DB::table('states')->insert([           
            'name' => 'TAS'          
        ]);
    }
}
