<footer class='footer'>
	 <div class="container">
		  <div class="row">
		      <div class="footer-mobile">
				  <div class="col-sm-3 footer-col-1"><img src="{{ url('') }}/images/site/logo-shm2.png" alt="Sinead Howard Millinery"></div>

				  <div class="col-sm-2 footer-col-2">
					  @if(count($modules))
						   <div class="footer-txt footer-menu"> <a href="{{ url('') }}">Home</a></div>

						   @foreach($modules as $nav_item)    
								<div class="footer-txt footer-menu"><a href="{{ url('') }}/{{ ($nav_item["name"] == "Pages" ? "pages/" : "") }}{{ $nav_item["slug"] }}">{{ $nav_item["display_name"] }}</a></div>
						   @endforeach
					   @endif
				  </div>
              </div>
                            
			  <div class="col-sm-3 footer-desktop"><img src="{{ url('') }}/images/site/logo-shm2.png" alt="Sinead Howard Millinery"></div>

			  <div class="col-sm-2 footer-desktop">
				  @if(count($modules))
					   <div class="footer-txt footer-menu"> <a href="{{ url('') }}">Home</a></div>

					   @foreach($modules as $nav_item)
						   	@if($nav_item["slug"]!='farmers')
							<div class="footer-txt footer-menu"><a href="{{ url('') }}/{{ ($nav_item["name"] == "Pages" ? "pages/" : "") }}{{ $nav_item["slug"] }}">{{ $nav_item["display_name"] }}</a></div>
						  	@endif
					   @endforeach
				   @endif
			  </div>              
              
			  <div class="col-sm-4 footer-col-3">
			    <div class="footer-txt"><a href="{{ url('pages/about/terms-and-conditions') }}" target="_blank">Terms and Conditions</a></div>
				<div class="footer-txt">@if ( $phone_number != "") <a href="tel:{{ str_replace(' ', '', $phone_number) }}"><i class='fa fa-phone'></i> {{ $phone_number }}</a>@endif</div>
				<div class="footer-txt">@if ( $email != "")<a href="mailto:{{ $email }}"><i class='far fa-envelope'></i> {{ $email }}</a>@endif </div>
				<div class="footer-txt">@if ( $social_instagram != "") <a href="{{ $social_instagram }}" target="_blank"><i class='fab fa-instagram'></i> {{ str_replace("https://", "", $social_instagram) }}</a>@endif</div>
				<div class="footer-txt">@if ( $social_facebook != "") <a href="{{ $social_facebook }}" target="_blank"><i class='fab fa-facebook'></i> Facebook - Sinead Howard Millinery</a>@endif</div>
			  </div>

			  <div class="col-sm-3 footer-col-4">
				 <div class="footer-txt">@if ( $company_name != "")<a href="{{ url('') }}">&copy; {{ date('Y') }} {{ $company_name }}</a>@endif </div>
				 <div class="footer-txt"><a href="https://www.echo3.com.au" target="_blank">Website by Echo3</a></div>
			  </div>
		   </div>
	  </div>
</footer>