<!-- Parallex 1 -->
<div class='parallax1-block'>
  <div class='parallax1-txt'>
	  {!! $home_intro_text !!}
	<a href="{{ url('') }}/contact"><div  class="parallax-container1-btn">Get In Touch</div></a>	
  </div>	
</div>

<div id="parallax1" class="parallaxParent1 parallax-container1">
	<div class="parallax1-img"></div>
</div>

<!-- Parallex 2 -->
<div class="parallax-container2">
	<div class="row">
	  <div class="column image-column parallax-container2-img1">  
	  </div>

	  <div class="column image-column parallax-container2-img2">    
	  </div>

	</div> 
</div>

<!-- Parallex 3 -->				
<div class='parallax3-block'>
  <!--
  <div class='parallax3-txt'>  	
	<h2><span>"A woman can be overdressed but never over elegant."</span></h2>
	<h3><span>- Coco Chanel</span></h3>		
  </div>	
  -->
</div>

<div id="parallax3" class="parallaxParent3 parallax-container3">
	<div class="parallax3-img"></div>
</div>										
	
							
@section('scripts')
<script type="text/javascript" src="{{ asset('/js/site/parallex/TweenMax.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/site/parallex/ScrollMagic.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/site/parallex/animation.gsap.js') }}"></script>
@endsection
										
@section('inline-scripts') 
	<script>
		// init controller
		var controller = new ScrollMagic.Controller({globalSceneOptions: {triggerHook: "onEnter", duration: "200%"}});

		// build scenes
		new ScrollMagic.Scene({triggerElement: "#parallax1"})
						.setTween("#parallax1 > div", {y: "80%", ease: Linear.easeNone})										
						.addTo(controller);
        //$(".parallax1-img").addClass('parallax1-img-mod');
		
		new ScrollMagic.Scene({triggerElement: "#parallax3"})
						.setTween("#parallax3 > div", {y: "80%", ease: Linear.easeNone})										
						.addTo(controller);
        //$(".parallax3-img").addClass('parallax3-img-mod');
		
	</script>
@endsection