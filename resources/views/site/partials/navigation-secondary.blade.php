<div class="div-navigation-secondary">	
   <a href='tel:{{ str_replace(' ', '', $phone_number) }}'><i class='fa fa-phone'></i> {{ $phone_number }}</a>
   <a href='{{ url('') }}/contact'><i class='far fa-envelope'></i></a>
   <a href='{{ $social_facebook }}' target="_blank"><i class='fab fa-facebook'></i></a>
   <a href='{{ $social_instagram }}' target="_blank"><i class='fab fa-instagram'></i></a>
   <a href='{{ url('') }}/cart/show'><i class='fas fa-shopping-cart'></i></a>
</div>