@if(isset($home_products))
     <div class="container marketing">   
		 <div class="home-products">
		   <div class="container">
			  <h2>New Looks</h2> 
			  <img src="{{ url('') }}/images/site/divider-top.png" title="Text Divider" alt="Text Divider" class="home-products-divider">

			  <div class="row">		            
				 @foreach($home_products as $item)       	 
					  <div class="col-lg-4">			           	           			           
						   @if (count($item->images) > 0)	
							  <a href='{{ url('') }}/products/{{ $item->category->slug }}/{{ $item->slug }}'>
								  <div class="home-products-a">
									 <div class="div-img">
										<img src="{{ url('') }}/{{$item->images[0]->location}}" alt="{{ $item->name }}">
									 </div>
									 <div class="home-products-txt">
										<h3>{{ $item->name }}</h3>						   				   					 				   				   					 
										<!-- <h4>${{ number_format($item->price,0) }}</h4> -->						   					   
									 </div>   
								   </div>
							  </a>   		             
						   @endif	
					  </div><!-- /.col-lg-4 -->
				 @endforeach 	

			</div>

			<a href="{{ url('') }}/products" class="home-products-btn">Show Me More</a>
		  </div>
		</div>
	</div>
@endif