<div class="parallax-container1 parallax-container1-overlay" data-parallax="scroll" data-position="top" ddata-bleed="10" data-image-src="{{ url('') }}/images/site/pic1.jpg" ddata-natural-width="1800" ddata-natural-height="540">
    <div class="parallax-container1-txt">         
		<h2>Made-to-order</h2>
		<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip</p>
		<a href="{{ url('') }}/contact"><div  class="parallax-container1-btn">Get In Touch</div></a>
	</div>	
</div>
   
<div class="parallax-container2">
<div class="row">
  <div class="column image-column parallax-container2-img1">  
  </div>
    
  <div class="column image-column parallax-container2-img2">    
  </div>

</div> 
</div>

<div class="parallax-container3" data-parallax="scroll" ddata-position="top" ddata-bleed="10" data-image-src="{{ url('') }}/images/site/pic4.jpg" ddata-natural-width="1800" dddata-natural-height="420">
    <div class="parallax-container3-txt"> 
		<h2><span>"A woman can be overdressed but never over elegant."</span></h2>
		<h3><span>- Coco Chanel</span></h3>
	</div>
</div>

@section('scripts')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>    
<script src="{{ asset('/components/parallax.js/parallax.js') }}"></script>
@endsection

 @section('inline-scripts')   
  <script>
      $(function(){		  
        if (navigator.userAgent.match(/(iPod|iPhone|iPad|Android)/)) {
          $('#ios-notice').removeClass('hidden');
          $('.parallax-container1').height( $(window).height() * 0.5 | 0 );
		  $('.parallax-container3').height( $(window).height() * 0.5 | 0 );
        } else {
          $(window).resize(function(){
            var parallaxHeight = Math.max($(window).height() * 0.7, 200) | 0;
			//$('.parallax-container1').height(parallaxHeight);  
            $('.parallax-container1').height(540);
			//$('.parallax-container1').width(1800);
			  
			$('.parallax-container3').height(parallaxHeight);
			//$('.parallax-container3').height(400);
          }).trigger('resize');
        }
      });
    </script>
@endsection