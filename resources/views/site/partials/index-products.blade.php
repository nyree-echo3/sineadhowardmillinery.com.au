@if(isset($home_products))		
		
        <div class="container marketing">   
		 <div class="home-products">		  
			  <h2>New Looks</h2> 
			  <img src="{{ url('') }}/images/site/divider-top.png" title="Text Divider" alt="Text Divider" class="home-products-divider">			
			  
			  <div class="products-carousel-panel">
					<div class="products-carousel" 
					   data-cycle-pause-on-hover="true"
					   data-cycle-fx="carousel"
					   data-cycle-timeout="4000"
					   data-cycle-carousel-visible="{{ $cycle_carousel_visible }}"
					   data-cycle-carousel-fluid="true"
					   data-cycle-slides="> span"
					 >

						 @foreach($home_products as $item)       	 					          
								 <span>								 	
								 	<div class="col-products">			           	           			           
									   @if (count($item->images) > 0)	
										  <a href='{{ url('') }}/products/{{ $item->category->slug }}/{{ $item->slug }}'>
											  <div class="home-products-a">
												 <div class="div-img">
													<img src="{{ url('') }}/{{$item->images[0]->location}}" alt="{{ $item->name }}">
												 </div>
												 <div class="home-products-txt">
													<h3>{{ $item->name }}</h3>						   				   					 				   				   					 
													<!-- <h4>${{ number_format($item->price,0) }}</h4> -->						   					   
												 </div>   
											   </div>
										  </a>   		             
									   @endif	
								    </div><!-- /.col-products -->							 
								 </span>					      
						 @endforeach 	

					   </div><!-- /.products-carousel -->					   
              </div> 
			
			  <a href="{{ url('') }}/products" class="home-products-btn">Show Me More</a>
					 
		</div><!-- /.home-products -->
	</div><!-- /.container marketing -->
	
@endif
    
@section('scripts-2')    
     <script src="{{ asset('/components/jquery-cycle2/build/jquery.cycle2.min.js') }}"></script>
     <script src="{{ asset('js/site/jquery.cycle2.carousel.js') }}"></script>
@endsection

@section('inline-scripts-2')  
   <script type="text/javascript">                    
	   $.fn.cycle.defaults.autoSelector = '.products-carousel';       
   </script>	   
@endsection
		
