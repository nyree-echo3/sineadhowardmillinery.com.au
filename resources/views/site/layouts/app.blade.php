@if(isset($mode) && $mode == 'preview')
{{$meta_title_inner = "Preview Page"}}
@endif

<!doctype html>
<html lang="en"> 
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="keywords" content="{{ (isset($meta_keywords_inner) ? $meta_keywords_inner : $meta_keywords) }}">
    <meta name="description" content="{{ (isset($meta_description_inner) ? $meta_description_inner : $meta_description) }}">
    <meta name="author" content="Echo3 Media">
    <meta name="web_author" content="www.echo3.com.au">
    <meta name="date" content="{{ $live_date }}" scheme="DD-MM-YYYY">
    <meta name="robots" content="all">
    <title>{{ (isset($meta_title_inner) ? $meta_title_inner : $meta_title) }}</title>

    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:300,400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Dosis:300,400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700" rel="stylesheet">

    <link href="{{ asset('/components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('/components/font-awesome/web-fonts-with-css/css/fontawesome-all.css') }}">
    <link href="{{ asset('/css/site/carousel.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/site/bootstrap-4-navbar.css') }}">
    <link rel="stylesheet" href="{{ asset('css/site/general.css') }}">

    <link rel="shortcut icon" href="{{ url('') }}/favicon.ico?">
    <link rel="apple-touch-icon" href="{{ url('') }}/apple-icon.png">
    @yield('styles')
    <!-- Google Analytics -->    
    {!! $google_analytics !!}

    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window,document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '328128171192636');
        fbq('track', 'PageView');
        @yield('fb-pixel-event')

    </script>
    <noscript>
        <img height="1" width="1" src="https://www.facebook.com/tr?id=328128171192636&ev=PageView&noscript=1"/>
    </noscript>
    <!-- End Facebook Pixel Code -->

</head>
<body>
<header>
    @include('site/partials/preview')
		@include('site/partials/logo')
		@include('site/partials/navigation')
		@include('site/partials/navigation-secondary')
</header>

<main role="main">
    @yield('content')
    @include('site/partials/footer')
</main>

<input type="hidden" id="hidPageType" name="hidPageType" value="{{ (isset($pageType) ? $pageType : "") }}">

<script src="{{ asset('/components/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('/components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/site/bootstrap-4-navbar.js') }}"></script>
<script src="{{ asset('js/site/logo-resize.js') }}"></script>
@yield('scripts')
@yield('inline-scripts')
@yield('scripts-2')
@yield('inline-scripts-2')
</body>
</html>
