<?php 
   // Set Meta Tags
   $meta_title_inner = $project_item->meta_title; 
   $meta_keywords_inner = $project_item->meta_keywords; 
   $meta_description_inner = $project_item->meta_description;  
?>
@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/baguettebox.js/src/baguetteBox.css') }}">
@endsection


<div class="blog-masthead ">         
    <div class="container">

      <div class="row">
        @include('site/partials/sidebar-projects')        
        
        <div class="col-sm-8 blog-main">
           <section class="project-block cards-project">
              <div class="container">	  
               
                 <div class="blog-post">                          
                    <h1 class="blog-post-title">{{ $project_item->title }}</h1>
            
						<div class='project-item'>
						   <div class='project-item-txt'>
							  {!! $project_item->description !!}
						   </div>

						   @if (count($project_item->images) > 0)
							   <div class='project-item-img'>
								   @foreach($project_item->images as $image)           	
										<div class="card border-0 transform-on-hover">	
											<div class='project-list-item-img'>
											    <a class="lightbox" href="{{ url('') }}{{$image->location}}" data-caption="{{$project_item->title}}">
													<img src="{{ url('') }}{{$image->location}}" alt="{{$project_item->name}}" class="card-img-top">
												</a>																				
											</div>
										</div>	
									@endforeach
								</div>
						   @endif

						</div>
         	     
          	        <div class='btn-back'>
           	           <a class='btn-back' href='{{ url('') }}/projects/{{ $project_item->category->slug }}'><i class='fa fa-chevron-left'></i> back</a>	
			       </div>
			   </div>
              </div>
           </section>      	   
          	 
           @include('site/partials/helper-sharing')	             					  								  			            
       </div><!-- /.blog-post -->         
    </div><!-- /.blog-main -->        

  </div><!-- /.row -->

</div><!-- /.container -->

@endsection


@section('scripts')
    <script src="{{ asset('/components/baguettebox.js/src/baguetteBox.js') }}"></script>
@endsection


@section('inline-scripts')
   <script type="text/javascript">
        $(document).ready(function () {       
           baguetteBox.run('.cards-project', { animation: 'slideIn'});
        });

        $(function() {
            $('#bookmarkme').click(function() {
                if (window.sidebar && window.sidebar.addPanel) { // Mozilla Firefox Bookmark
                    window.sidebar.addPanel(document.title,window.location.href,'');
                } else if(window.external && ('AddFavorite' in window.external)) { // IE Favorite
                    window.external.AddFavorite(location.href,document.title);
                } else if(window.opera && window.print) { // Opera Hotlist
                    this.title=document.title;
                    return true;
                } else { // webkit - safari/chrome
                    alert('Press ' + (navigator.userAgent.toLowerCase().indexOf('mac') != - 1 ? 'Command/Cmd' : 'CTRL') + ' + D to bookmark this page.');
                }
            });
        });
    </script>			
@endsection