<!DOCTYPE html>
<html>
<body>
<img src="{{ url('') }}/images/site/email-logo.png"><br/>
<table class="table">
    @foreach(json_decode($contact_message->data) as $field)
        <tr>
            <th style="width:20%">{{ strtoupper($field->field) }} :</th>
            <td>{{ $field->value }}</td>
        </tr>
    @endforeach
</table>
<br/><img src="{{ url('') }}/images/site/email-thanks.png">
</body>
</html>
